(ns lemondronor.advisorycircular.twitter
  (:require
   ["fs" :as fs]
   [kitchen-async.promise :as p]
   [lemondronor.advisorycircular.logging :as logging]
   [lemondronor.advisorycircular.util :as util]
   ["twit" :as Twit]))

(def fsprom (.-promises fs))

(declare logger log-debug log-verbose log-info log-warn log-error)
(logging/deflog "twitter" logger)


;; Creates a new twit object.

(defn twit [config]
  (Twit. (clj->js {:consumer_key (:consumer-key config)
                   :consumer_secret (:consumer-secret config)
                   :access_token (:access-token config)
                   :access_token_secret (:access-token-secret config)})))


;; Uploads an image to twitter. Returns a promise that resolves to the
;; new media ID of the image.

(defn upload-image [twit path]
  (log-info "Uploading media to twitter: %s" path)
  (p/let [b64content (util/read-file path {:encoding "base64"})
          result (.post twit "media/upload" (clj->js {:media_data b64content}))
          media-id (get-in (js->clj result :keywordize-keys true) [:data :media_id_string])]
    (log-info "%s got media ID %s" path media-id)
    media-id))


;; Posts a tweet with optional multiple media. Returns a promise that
;; resolves to the response result.

(defn tweet [twit status image-paths lat lon]
  (p/then (p/all (map #(upload-image twit %) image-paths))
          (fn [media-ids]
            (log-warn "Tweeting status:'%s' with media: %s" status media-ids)
            (p/let [result (.post twit "statuses/update"
                                  (clj->js {:status status
                                            :media_ids [media-ids]
                                            :lat lat
                                            :long lon
                                            :display_coordinates true
                                            }))
                    result (js->clj result :keywordize-keys true)]
              (log-debug "Tweet posted: %s" result)
              result))))
